function upgrade
	yay -Suy
	yay -Yc
	paccache -r
	fwupdmgr refresh
	fwupdmgr get-updates
	pacman -Qm > ~/.config/pacman/nativePackages.txt
	pacman -Qn > ~/.config/pacman/AURPackages.txt
end
