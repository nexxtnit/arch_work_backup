# Start X at login
if status is-login
    if test -z "$DISPLAY" -a $XDG_VTNR = 1
        exec startx -- -keeptty
    end
end

#bobthefish theme config
set -g theme_color_scheme solarized-dark
set pipenv_fish_fancy yes