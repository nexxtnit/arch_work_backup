#!/usr/bin/env python3

import sys

filename = "/etc/pacman.conf"
#filename = "/home/david/.config/pacman/pacman.conf"


if len(sys.argv) != 2:
    print("pass string as argument")
    sys.exit()
else:
    # read input file
    pacman_conf_new = []

    with open(filename, "r") as pacmanconf_system:
        pacman_conf_new = pacmanconf_system.readlines()

    for i, line in enumerate(pacman_conf_new):
        if line.startswith("IgnorePkg"):
            ignorepkg = line.rsplit()
            if sys.argv[1] in ignorepkg:
                print(str(sys.argv[1] + " is already on ignore list"))
                sys.exit(0)
            ignorepkg.append(sys.argv[1] + "\n")
            pacman_conf_new[i] = " ".join(ignorepkg)

    with open(filename, "w") as pacmanconf_system:
        pacmanconf_system.writelines(pacman_conf_new)
        print(str(sys.argv[1] + " added to pacman conf"))
