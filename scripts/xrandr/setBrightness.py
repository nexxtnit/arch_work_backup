#!/usr/bin/env python3
import argparse
import subprocess

parser = argparse.ArgumentParser()
parser.add_argument("changeVal")
args = parser.parse_args()


# read current gamme
gammaVar=subprocess.run('xrandr --verbose | grep -oP "(?<=Gamma:      ).*"',shell=True,capture_output=True,text=True)
gamma=gammaVar.stdout.strip()

# read current brightness
brightnessVar=subprocess.run('xrandr --verbose | grep -oP "(?<=Brightness: ).*"',shell=True,capture_output=True,text=True)
brightness=brightnessVar.stdout.strip()

#compute new brightness
brightnessNew=float(brightness)+(float(args.changeVal)/100) 

# check if new brightness is between 0 and 1
brightnessNew = min(brightnessNew, 1);
brightnessNew = max(brightnessNew, 0);

# execute command to set brightness with current gamma
cmd="xrandr --output HDMI-0 --gamma "+str(gamma)+" --brightness "+str(brightnessNew)
subprocess.run(cmd,shell=True)

# print(f"gamma: {gamma}")
# print(f"brightness: {brightness}")
# print(f"brightnessNew: {brightnessNew}")