[Appearance]
ColorScheme=Tango+solarized_dark
Font=Input Mono,18,-1,5,57,0,0,0,0,0,Medium
TabColor=255,255,255

[General]
Name=David
Parent=FALLBACK/

[Interaction Options]
MouseWheelZoomEnabled=true
TextEditorCmd=6
TextEditorCmdCustom=code PATH:LINE:COLUMN

[Scrolling]
HistorySize=10000
